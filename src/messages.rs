use rand::Rng;
use serenity::{
    async_trait,
    model::{channel::Message, gateway::Ready},
    prelude::*,
};
pub struct Handler;
#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, ctx: Context, msg: Message) {
        if msg.content == "$man" {
            let dm = msg
                .author
                .dm(&ctx, |m| {
                    m.content("```linuxlandia(1)\n  linuxlandia - Discord Server ```");
                    m
                })
                .await;

            if let Err(why) = dm {
                println!("Error when direct messaging user: {:?}", why);
            }
        }
        if msg.content == "$gayness" {
            let gayness = rand::thread_rng().gen_range(1, 101);
            let output = format!("Your Gayness is: {}%", gayness);
            if let Err(why) = msg.channel_id.say(&ctx.http, output).await {
                print!("Error: {:?}", why);
            }
        }
        if msg.content.contains("minarquismo") && msg.author.name != "linuxlandia-bot" {
            if let Err(why) = msg.channel_id.say(&ctx.http, "En el minarquismo las empresas no quieren arriesgar, entonces coperan con la sociedad para luchar").await {
                print!("Error: {:?}", why);
            }
        }
    }
    async fn ready(&self, _: Context, ready: Ready) {
        println!("{} is connected!", ready.user.name);
    }
}
